package com.epam.course.renchka;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSMS {

  public static final String ACCOUNT_SID = "ACe0cf63178b045c56220c93d828ba9f93";
  public static final String AUTH_TOKEN = "bb2eeaa3ccea069d8eaf7e8830559bb8";

  public static void send(String str) {
    Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
    Message message = Message
        .creator(new PhoneNumber("+380974207431"),
            new PhoneNumber("+17024877047"), str)
        .create();
  }
}
